package HashMap;

import java.util.HashMap;

/**
 * 阿里一流公司  面试非常注重基础 底层
 *  二流 项目经验
 */
public class HashMap_Demo {

    /**
     * 在a^n中,a叫做底数,n叫做指数。a^n读作“a的n次方”或“a的n次幂“。
     * DEFAULT_LOAD_FACTOR = 0.75f;
     *
     * 1.初始化数组， 默认初始容量 16
     * DEFAULT_INITIAL_CAPACITY = 1 << 4; CAPACITY.容量 // aka 16    MUST be a power of two.  a^n    a的n次幂 如果初始容量不是 强转
     * DEFAULT_LOAD_FACTOR = 0.75f;   FACTOR 因子
     *
     * 1. 怎么放到数组？  key的hashCode() 计算出哈希值  0-15  取模的方式 hash位运算16 取余数  java中采用位运算  计算值 确定放入table
     *
     *      对key值 计算hash()值 取余位运算010101  计算出下标  如果计算的值一样 链表 jdk1.7 头插入 hash 链表长 利用率不高 hash散列
     *
     * 2.为何容量是2的指数次幂？ 因为计算哈希值是 16的位运算取模  为什么扩载因子是0.75？
     *
     *      扩容越多，hash冲突  0.75 空间时间利用率是最高的
     *    static final int hash(Object key) {
     *         int h;
     *         return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
     *     }
     *     >>：带符号右移。正数右移高位补0，负数右移高位补1
     *     >>>：无符号右移。无论是正数还是负数，高位通通补0。
     *
     *      static final int tableSizeFor(int cap) {
     *         int n = cap - 1;
     *         n |= n >>> 1;
     *         n |= n >>> 2;
     *         n |= n >>> 4;
     *         n |= n >>> 8;
     *         n |= n >>> 16;
     *         return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
     *     }
     *
     *
     * 3.如果hash值取模后相等？   产生哈希冲突（碰撞）  这个时候引入链表 头部插入java7 后入第一个
     *
     * 扩容 rehash  位运算 性能提高
     *
     * 4.为什么hash是线程不安全的？ 数据丢失 死锁2
     *  hashmap7 扩容的时候会有死锁     java8 链表扩容思想  迁移避免死锁 loHead LoTail  hiHead hiTail
     *
     *
     * 5.链表超过8时候会转换成红黑树  当数组长度<64的时候，优先扩容  tab.length< min_tree_capacity  当大于64 转换成红黑树
     *  链表转红黑树，阈值8 实际值是9
     *
     *
     */
    public static void main(String[] args) {
        HashMap hashMap=new HashMap();
        hashMap.put("1","2");

    }
}
