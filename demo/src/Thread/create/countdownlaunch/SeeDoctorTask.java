package Thread.create.countdownlaunch;

import java.util.concurrent.CountDownLatch;

public class SeeDoctorTask implements Runnable {

    CountDownLatch countDownLatch;

    public SeeDoctorTask(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {
        try {
            System.out.println("开始看医生任务");
            Thread.sleep(2000);
            System.out.println("看医生结束，准备离开病房");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (countDownLatch != null) {
                countDownLatch.countDown();
            }
        }
    }
}
