package Thread.create.countdownlaunch;

import java.util.concurrent.CountDownLatch;

public class QueueTask implements Runnable {

    CountDownLatch countDownLatch;

    public QueueTask(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public void run() {

        try {
            System.out.println("开始在医院药房排队买药逻辑");
            Thread.sleep(300);
            System.out.println("排队成功，可以开始缴费买药");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (countDownLatch != null)
                countDownLatch.countDown();//一个线程完成了自己的任务后，计数器的值就会减1。当计数器值到达0时，
        }

    }
}
