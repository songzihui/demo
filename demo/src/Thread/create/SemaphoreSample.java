package Thread.create;

import javafx.concurrent.Task;

import java.util.concurrent.Semaphore;

public class SemaphoreSample {

    public static void main(String[] args) {
        Semaphore semaphore=new Semaphore(3);
        for (int i = 0; i <4 ; i++) {
                new Thread(new Task(semaphore,"11")).start();

        }
    }


  static class Task extends  Thread{

      Semaphore semaphore;
      public Task(Semaphore semaphore,String name) {
          this.semaphore = semaphore;
          this.setName(name);
      }
      public void run(){
          try {
              semaphore.acquire(); //acquire() 表示阻塞并获取许可

              System.out.println(Thread.currentThread().getName()+"aquire() 的时间"+System.currentTimeMillis());
              semaphore.release();//release() 表示释放许可

          }catch (InterruptedException e){
             e.printStackTrace();
          }
      }
  }

}
