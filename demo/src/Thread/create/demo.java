package Thread.create;

import java.util.concurrent.*;

class ThreadDemo {

    static class Thread extends java.lang.Thread {
        @Override
        public void run() {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + ":" + i);
            }
        }
    }
}

   class RunnableDemo implements java.lang.Runnable{

        @Override
        public void run() {
            for (int i = 0; i <10 ; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
        }
    }


    class CallableDemo implements Callable<String>{

        @Override
        public String call() throws Exception {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName()+":"+i);
            }
            return "hello 看我";
        }
    }


public class demo{
    public static void main(String[] args) {
        // 1
        Thread thread =new Thread();
        thread.start();
        // 2
        RunnableDemo runnableDemo=new RunnableDemo();
        new Thread(new RunnableDemo()).start();
        // 3
        CallableDemo callableDemo=new CallableDemo();
        FutureTask<Integer> futureTask = new FutureTask(callableDemo);
        //   new Thread(futureTask).start();
        new Thread(new FutureTask<String>(callableDemo)).start();

        //4线程池
        ExecutorService executor = Executors.newFixedThreadPool(5);
        for (int i=0;i<20;i++){
            executor.execute(new RunnableDemo());
            executor.submit(new RunnableDemo());
        }

    }
}



