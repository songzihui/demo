package demo02;

import java.util.concurrent.atomic.AtomicInteger;

public class Test_AtomicInteger {

    static AtomicInteger threadNo= new AtomicInteger(1);


    public static void main(String[] args) {
        char aI[] = "1234567".toCharArray();
        char aC[] = "ABCDEFG".toCharArray();

        new Thread(() -> {
            for(char c:aI){
                while (threadNo.get()!= 1){}            //自旋锁 线程不放弃cup 不经过操作系统 正常情况用户态  去内核态申请
                System.out.println(c);
                threadNo.set(2);
            }
        }, "t1").start();

        new Thread(()->{
            for (char c:aC){
                while (threadNo.get()!= 2){}
                System.out.println(c);
                threadNo.set(1);
            }
        });

    }
}
