package demo02;

/**
 * 两个线程，一个输出字母，一个输出数字，交替输出  考察线程通信的知识
 */
public class LockSupport {
    static Thread t1 = null;
    static Thread t2 = null;
//    static Thread t1 =null,t2=null;


    public static void main(String[] args) {
        char aI[] = "1234567".toCharArray();
        char aC[] = "ABCDEFG".toCharArray();

//        System.out.println(a);
//        System.out.println(b);

        t1 = new Thread(() -> {
            for (char c : aI) {
                System.out.println(c);
                java.util.concurrent.locks.LockSupport.unpark(t2);//unpark(t2) 让t2继续运行
                java.util.concurrent.locks.LockSupport.park(); //当前线程暂停  阻塞
            }

        }, "t1");

        t2 = new Thread(() -> {
            for (char c : aC) {
                java.util.concurrent.locks.LockSupport.park(); //阻塞当前线程 t2
                System.out.println(c);// 先阻塞不去循环 输出当前
                java.util.concurrent.locks.LockSupport.unpark(t1);//
            }
        });

        t1.start();
        t2.start();
    }
}
