package demo02;

/**
 * 线程 多个
 * 进程 一个应用程序
 */

/**
 * 自旋锁
 */
public class Cas {

    enum ReadyToRun {T1, T2}

    static volatile ReadyToRun r = ReadyToRun.T1;

    public static void main(String[] args) {
        char aI[] = "1234567".toCharArray();
        char aC[] = "ABCDEFG".toCharArray();

        new Thread(() -> {
            for(char c:aI){
                while (r!=ReadyToRun.T1){}            //自旋锁 线程不放弃cup 不经过操作系统 正常情况用户态  去内核态申请
                System.out.println(c);
                r=ReadyToRun.T2;
            }
        }, "t1").start();

        new Thread(()->{
            for (char c:aC){
                while (r!=ReadyToRun.T2){}
                System.out.println(c);
                r=ReadyToRun.T1;
            }
        });

    }
}
