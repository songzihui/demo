package demo02;


/**
 * 想用wait 和notify 必须加锁 synchronize  锁定的是对象
 *
 * notify a 线程持有锁   叫醒等待队列里的的某一个线程  不叫醒处于死锁状态
 *
 * wait 当前线程让出锁
 *
 */
public class sycn_wait_notify {

    public static void main(String[] args) {
        final Object o = new Object();

        char aI[] = "1234567".toCharArray();
        char aC[] = "ABCDEFG".toCharArray();

        new Thread(() -> {
            synchronized (o) {
                for (char c : aI) {
                    System.out.println(c);//输出
                    try {
                        o.notify();//叫醒第二个线程
                        o.wait();//自己等待
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                o.notify();
            }
        }, "t1").start();


        new Thread(() -> {
            synchronized (o) {
                for (char c : aC) {
                    System.out.println(c);
                }
                try {
                    o.notify();
                    o.wait();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                o.notify();
            }
        }, "t2").start();
    }
}
