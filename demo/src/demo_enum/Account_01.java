package demo_enum;

public  class Account_01 {
    public static final Account_01 ACCOUNT_01=new Account_01(),
    SAVING=new Account_01(),
    FIXED=new Account_01(),
    CURRENT=new Account_01();

    private Account_01() {
        System.out.println("----------");
    }

    public static void main(String[] args) {
        System.out.println(Account_01.SAVING);
    }
}
