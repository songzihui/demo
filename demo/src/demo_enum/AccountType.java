package demo_enum;


/**
 * 也就是枚举类不给外界实例化的机会，只能它自己实例化，而一个枚举类的所有实例就只有枚举前面分号前的那几个，其他地方不允许创建。
 */
public class AccountType {
    enum Type {
        SAVING, FIXED, CURRENT;

        //构造方法是私有的，枚举中    枚举与类的根本区别，就在于构造方法私有。
       private Type() {
            System.out.println("---------------");
        }
    }

     AccountType() {
    }
    public static void main(String[] args) {

        //Type type=new Type(); 不能new实例
        System.out.println(Type.CURRENT);
    }
}
