package sql_index;

/**
 *
 *  Mysql的隔离级别
 *  Read Uncommitted（读取未提交内容）
 *  Read Committed（读取提交内容）
 *  Repeatable Read（可重读）
 *  Serializable（可串行化）
 *
 *
 * 索引是帮助mysql高效获取数据的数据结构  索引底层是数据结构
 *  二叉树
 *  红黑树
 *  hash表
 *  B+Tree
 *  1.非叶子节点不存data，只存储key
 *  2.叶子节点不存储指针
 *  3.顺序访问指针，key从左到右递增    叶子节点存所有的数据-非叶子节点做了冗余 16k  1170  3次磁盘io存2千万个
 *
 *  mysql的表 两种存储引擎 InnoDB、MyISAM  形容表的
 *   InnoDB 是聚集索引，MyISAM 是非聚集索引。
 *   InnoDB 支持事务，MyISAM 不支持事务。
 *   InnoDB 支持外键，而 MyISAM 不支持。
 *   InnoDB 不保存表的具体行数，执行 select count(*) from table 时需要全表扫描。而MyISAM 用一个变量保存了整个表的行数，执行上述语句时只需要读出该变量即可，速度很快；
 *
 *   MyIsAM  存储  1. frm 对应表结构   2. MYD 表的数据   3. MYI 表所在的索引字段存储的位置  非聚集索引  要从MYI文件去查MYD文件所在的数据
 *   在MyIsAM存储引擎中       B+Tree 子节点存的是磁盘文件所在行的指针0X07
 *
 *   InnoDB 存储 1.frm对应的表结构 2.ibd 存储数据和索引   ibd 表数据文件 本身就是按B+Tree组织的一个索引文件
 *   叶子节点包含了完整的数据记录
 *   InnoDB必须有主键 并推荐是整型自增 ？原因：1.索引比大小 整型比大小比uuid效率更高 2.防止分裂从后面插入 从后面新开辟插入
 *   指针
 *   节点节点之间有指针：类似hash定位元素后通过 遍历所以能查找范围内的data节点
 *
 */
public class index {



}
