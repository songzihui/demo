package demo_abstract;


/**
 * 如果一个类中有一个抽象方法，那么当前类一定是抽象类；抽象类中不一定有抽象方法。
 *
 * 抽象类不能被实例化，抽象类和抽象方法必须被abstract修饰 不能被new
 */
public abstract class Animal_01 {

    //可以定义静态方法
    public static String c="1";
    //抽象类中可以存在普通属性，方法，静态属性和方法。
    int num=5;

    //可以定义抽象方法 访问权限不能是private
    public abstract String eat();

    //可以定义普通方法，方法的访问类型可以随便定义private public  protected
    private String sleep(){
        return "打呼噜";
    }

    //可以定义构造方法
    public Animal_01() {
    }
}
