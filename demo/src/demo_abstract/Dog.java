package demo_abstract;

public class Dog extends Animal_01  implements Animal_Interface{

    //@Override 抽象类中的抽象方法，需要有子类实现，如果子类不实现，则子类也需要定义为抽象的。 抽象类中的方法必须全部实现
    public String eat() {
        return "吃骨头";
    }


    public static void main(String[] args) {
        //new一个t1线程
        new Thread(()->{

        },"t1");
    }

    //继承接口，接口中的方法都要实现
    @Override
    public int AnimalsHand() {
        return 0;
    }
}
