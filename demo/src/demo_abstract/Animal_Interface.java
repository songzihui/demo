package demo_abstract;


/**
 * 4.接口中没有构造方法，也不能实例化接口的对象。（所以接口不能继承类）
 *
 * 5.接口可以实现多继承
 *
 * 6.接口中定义的方法都需要有实现类来实现，如果实现类不能实现接口中的所有方法则实现类定义为抽象类。
 */
public interface Animal_Interface {

    //在接口中只有常量，因为定义的变量，在编译的时候都会默认加上public static final   接口中的访问类型只能是public protected
    public static final String a="1";


    //在接口中的方法，永远都被public来修饰。 思考：接口是让别人使用的，拿来继承的
    public int AnimalsHand();

}
