package demo01;

import java.util.Scanner;
/** 加减乘除*/
public class Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        if (str.contains("+")) {
            int i = str.indexOf("+");
            String substring = str.substring(0, i);
            String substring2 = str.substring(i + 1);
            System.out.println(Integer.parseInt(substring) + Integer.parseInt(substring2));
        } else if (str.contains("-")) {
            int i = str.indexOf("-");
            String substring = str.substring(0, i);
            String substring2 = str.substring(i + 1);
            System.out.println(Integer.parseInt(substring) - Integer.parseInt(substring2));
        } else if (str.contains("*")) {
            int i = str.indexOf("*");
            String substring = str.substring(0, i);
            String substring2 = str.substring(i + 1);
            System.out.println(Integer.parseInt(substring) * Integer.parseInt(substring2));
        } else if (str.contains("/")) {
            int i = str.indexOf("/");
            String substring = str.substring(0, i);
            String substring2 = str.substring(i + 1);
            System.out.println(Integer.parseInt(substring) / Integer.parseInt(substring2));
        }

    }
}

