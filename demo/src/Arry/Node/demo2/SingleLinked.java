package Arry.Node.demo2;

public class SingleLinked {

    public static void main(String[] args) {

        Node node1 = new Node(1, "111");
        Node node2 = new Node(2, "222");
        Node node3 = new Node(3, "333");

        SingleLinkList singleLinkList = new SingleLinkList();
        singleLinkList.add(node1);
        singleLinkList.add(node2);
        singleLinkList.add(node3);
        singleLinkList.list();
    }

}

class SingleLinkList {

    private Node head = new Node(0, " ");


    public void add(Node node) {
        //辅助一个temp为头节点
        Node temp = head;
        while (true) {
            //找到链表最后
            if (temp.next == null) {
                break;
            }
            //如果没有找到最后
            temp = temp.next;
        }
        //当退出while循环 指向最后
        temp.next = node;
    }


    public void list() {
        if (head.next == null) {
            System.out.println("链表为空");
            return;
        }
        Node temp = head.next;
        while (true) {
            if (temp == null) {
                break;
            }
            System.out.println(temp);
            temp = temp.next;
        }
    }

}


class Node {
    public int no;
    public String name;
    public Node next;


    public Node() {
    }

    @Override
    public String toString() {
        return "Node{" +
                "no=" + no +
                ", name='" + name + '\'' +
                '}';
    }

    public Node(int no, String name) {
        this.no = no;
        this.name = name;
    }

}


