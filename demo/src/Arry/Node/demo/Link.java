package Arry.Node.demo;

public class Link {
    Node head;

    public Link() {
        head = new Node();
    }


    public void add(String data) {
        Node node = new Node(data);
        Node temp = head;
        while (temp.next != null) {
            temp = temp.next;//下一个节点放在当前节点

        }
        temp.next = node;//头节点放的是下一个节点
    }

    public void del(String data) {
        Node temp = head;
        while (temp.next != null) {
            if (temp.next.name.equals(data)) {
                temp.next = temp.next.next;
                break;
            }
            temp=temp.next;
        }
    }

    public int size() {
        int len = 0;
        Node temp = head;
        while (temp.next != null) {
            len++;
            temp = temp.next;
        }
        return len;
    }

    public Node find(String name) {
        Node temp = head;
        Node data = null;
        while (temp.next.next != null) {
            if (temp.name.equals(name)) {
                data = temp.next;
                break;
            }
            temp = temp.next;
        }
        return data;
    }

    public void display() {
        Node temp = head;
        while (temp.next != null) {
            System.out.print(temp.next.name + "===");
            temp = temp.next;
        }
    }

    public static void main(String[] args) {
        Link link = new Link();
        link.add("组长");
        link.add("经理");
        link.add("副总");
        link.add("总经理");
        link.display();
        link.del("副总");
        System.out.println(" ");
        link.display();
    }
}
